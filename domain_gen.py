import scrapy
import json
import whois

class DomainSpider(scrapy.Spider):
    name = 'domainspider'

    def __init__(self, keyword='', *args, **kwargs):
        super(DomainSpider, self).__init__(*args, **kwargs)
        self.start_urls = ['http://www.businessnamegenerators.com/index.php?word=' + keyword]

    def parse(self, response):
        name_data = json.loads(response.body)

        response2 = response.replace(body = name_data["html"])
        #return {'html': name_data["html"]}
        names = response2.xpath('//div[@style="margin-left:20px"]/text()').extract()

        for dname in names:
            domain = dname + '.com'
            print('Trying: ' + domain)
            try:
                whois.whois(domain)
            except whois.parser.PywhoisError:
                print('!!! Found: ' + domain)
                yield {'domain': domain}


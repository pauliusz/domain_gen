#!/bin/sh

if [ -z $1 ]; then
	scrapy runspider domain_gen.py -o domains.csv -t csv --nolog
else
	scrapy runspider domain_gen.py -a keyword=$1 -o domains.csv -t csv --nolog
fi

awk '!seen[$0]++' domains.csv > domains_unique.csv
mv domains_unique.csv domains.csv
